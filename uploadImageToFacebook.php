<?php

require_once 'sdk/facebook-php-sdk-v4-4.0-dev/autoload.php';
require_once 'saveImageClass.php';

use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\GraphObject;

if($_SERVER['REQUEST_METHOD'] == "POST") {
	if(isset($_POST)) {
		
		$saveImage = new SaveImage($_POST['image_data'], $_POST['username']);
		$resultOfSavingImage = $saveImage->writeToFile();

		if(!$resultOfSavingImage[0]) {
			echo "upload saveImage->writeToFile: False!! ";
			//echo "false";	
			die();
		}
		else {
			$filename = $resultOfSavingImage[1];
			//echo $filename;
			FacebookSession::setDefaultApplication('856009331099581', 'bce64744488ad081f9436db90ce66e00');
			$session = new FacebookSession($_POST['access_token']);
			
			try {
				$session->validate();
			}
			catch(FacebookRequestException $exp) {
				echo $exp->getMessage();
				die();
			}
			catch(Exception $exp) {
				echo $exp->getMessage();
				die();
			}
//			echo $filename;
//			echo "<br/>";
//			echo realpath($filename);
			
			try {
				//echo " all good.. uploading to FB.. ";
				$request = new FacebookRequest(
					$session, 
					'POST', 
					'/me/photos',
					array(
						'source' => new CURLFile(realpath($filename), 'image/png'),
						//'message' => "Uploaded via Meme Builder",
						//'no_story' => true
					)
				);
				$response = $request->execute();
				$obj = $response->getGraphObject();
				echo $obj->getProperty('id');
			}
			catch(FacebookRequestException $exp) {
				echo 'false';
				echo $exp->getMessage();
				die();
			}
			catch(Exception $exp) {
				echo 'false';
				echo $exp->getMessage();
				die();
			}
		}	
	}
}
?>