/*
*
*
*
*
*
*/

$('#welcomeBox').modal({backdrop: 'static'});
$("#welcomeBox").modal('show');
$("#rightToolPalletButton").hide();
$("#closeRightToolPallet").hide();
$(".color-picker").hide();
$("#brushToolPalate").hide();
$("#popUpContainer").hide();

/*================= Change Canvas Size =====================*/

$(document).on('click', '#canvasSize li', function() {
	$("#rightToolPalate").addClass("col-md-5");
	$("#rightToolPalate").removeClass("floatMenu");
	$("#rightToolPalletButton").hide();
	switch($(this).prop("id")){
		case "canvasSize1" : 
		canvasWidth = 500;
		canvasHeight = 500;
		canvas.setDimensions({width: canvasWidth, height: canvasHeight});
		$("#rightToolPalate").show();
		break;

		case "canvasSize2" : 
		canvasWidth = 800;
		canvasHeight = 500;
		canvas.setDimensions({width: canvasWidth, height: canvasHeight});
		floatingRightTools();
		$('#popUpContainer').fadeIn('slow').delay(4000).fadeOut('slow');
		break;

		case "canvasSize3" : 
		canvasWidth = 500;
		canvasHeight = 800;
		canvas.setDimensions({width: canvasWidth, height: canvasHeight});
		$("#rightToolPalate").show();
		break;

		case "canvasSize4" : 
		canvasWidth = 800;
		canvasHeight = 800;
		canvas.setDimensions({width: canvasWidth, height: canvasHeight});
		floatingRightTools();
		$('#popUpContainer').fadeIn('slow').delay(4000).fadeOut('slow');
		break;
	}
});

function floatingRightTools(){
	$("#rightToolPalate").hide();
	$("#rightToolPalate").removeClass("col-md-5");
	$("#rightToolPalate").addClass("floatMenu");
	$("#rightToolPalletButton").show();
}

$(document).on('click', '#rightToolPalletButton', function() {
	$("#rightToolPalletButton").hide();
	$("#closeRightToolPallet").show();
	$("#rightToolPalate").show("slide", { direction: "right" }, 1000);

	setTimeout(function(){
		var bLazy2 = new Blazy({ 
			breakpoints: [{
				width: 420 /* Max-width */
				, src: 'data-src-small'
			}]
			, selector: '.stockImageContainer, img'
			, container: '#blankMemesContainer' /* Default is window */
			, offset : 1
			, success: function(element){
				setTimeout(function(){
					/* We want to remove the loader gif now. */
					/* First we find the parent container */
					/* then we remove the "loading" class which holds the loader image */
					var parent = element.parentNode;
					parent.className = parent.className.replace(/\bloading\b/,'');
				}, 1000);
			}
		});
	}, 1000);

});

$(document).on('click', '#closeRightToolPallet', function() {
	$("#closeRightToolPallet").hide();
	$("#rightToolPalate").hide("slide", { direction: "right" }, 1000);
	$("#rightToolPalletButton").show();
});




/*================== Load Memes =============================*/

function loadBlankMemes(){
	setTimeout(function() {
		$.getJSON("blankMemes.json", function(data) {
			var blankMemesTemp = "";
			for(var i = 0; i < data.length; i++) {

				blankMemesTemp = "<div class='stockImageContainer'>" +
				"<div class='stockImageOverlay'></div>" + 
				"<img class='stockImage b-lazy' src='images/loader.gif' data-src='images/blankMemes/" + data[i].url + "' id='" + data[i].id + "' alt='alt-text'> " +
				"</div>";

				$("#blankMemesContainer").append(blankMemesTemp);
			}
		});
	}, 1000);

}


window.addEventListener("load", loadBlankMemes);


function loadMemeTemplates(){
	setTimeout(function() {
		$.getJSON("templates.json", function(data) {

			var blanktemplatesTemp = "";
			for(var i = 0; i < data.length; i++) {

				blanktemplatesTemp = "<div class='stockImageContainer'>" +
				"<div class='stockImageOverlay'></div>" + 
				"<img class='stockImage b-lazy' src='images/loader.gif' data-src='images/templates/" + data[i].url + "' id='" + data[i].id + "'> " +
				"</div>";

				$("#memeTemplatesContainer").append(blanktemplatesTemp);
			}
		});
	}, 1000);

}

window.addEventListener("load", loadMemeTemplates);

function loadSpeechBubbles(){

	setTimeout(function() {
		$.getJSON("speechbubbles.json", function(data) {

			var blankspeechBubblessTemp = "";
			for(var i = 0; i < data.length; i++) {

				blankspeechBubblesTemp = "<div class='stockImageContainer'>" +
				"<div class='stockImageOverlay'></div>" + 
				"<img class='stockImage b-lazy' src='images/loader.gif' data-src='images/speechbubbles/" + data[i].url + "' id='" + data[i].id + "'> " +
				"</div>";

				$("#speechbubblesContainer").append(blankspeechBubblesTemp);
			}
		});
	}, 1000);

}

window.addEventListener("load", loadSpeechBubbles);

/*============================= Load / Hide Color Pickers ==============================================*/

/* Canvas Color Picker */
$("#canvasColor").click(function(evt) {
	$(".color-picker").hide();
	$("#selectCanvasColor").show();	
});

/* In Add Text Box */
$("#textFillColor").click(function(evt) {
	$(".color-picker").hide();
	$("#selectTextFillColor").show();	
});

$("#textStrokeColor").click(function(evt) {
	$(".color-picker").hide();
	$("#selectTextStrokeColor").show();	
});

$("div#addTextContainer").click(function(evt) {

	if(evt.target.id == "textFillColor")
		return;
	if($(evt.target).closest('#textFillColor').length)
		return;    
	if(evt.target.id == "textStrokeColor")
		return;
	if($(evt.target).closest('#textStrokeColor').length)
		return; 

	if(evt.target.id == "selectTextFillColor")
		return;
	if($(evt.target).closest('#selectTextFillColor').length)
		return;       
	if(evt.target.id == "selectTextStrokeColor")
		return;
	if($(evt.target).closest('#selectTextStrokeColor').length)
		return;         


	$(".color-picker").hide();
});


/* Brush Tool Palate */
$("#brushColor").click(function(evt) {
	$(".color-picker").hide();
	$("#selectBrushColor").show();	
});


/*=== Main page ===*/

$("#fillColor").click(function(evt) {

	if((canvas.getActiveObject() !== undefined) && (canvas.getActiveObject() !== null)){
		if(canvas.getActiveObject().get("type") === "circle"   || 
			canvas.getActiveObject().get("type") === "rect"     ||
			canvas.getActiveObject().get("type") === "triangle" ||
			canvas.getActiveObject().get("type") === "line") 
		{
			$("body").unbind("keydown");
			$(".color-picker").hide();
			$("#fillColorContainer").show();
			$("#selectFillColor").show();
			
		}else{
			$("#customMessage p").html("<span class='fa fa-info-circle'></span> Please select a shape");
			$('#customMessage').fadeIn('slow').delay(2000).fadeOut('slow');
		}
	}else{
		$("#customMessage p").html("<span class='fa fa-info-circle'></span> Please select a shape");
		$('#customMessage').fadeIn('slow').delay(2000).fadeOut('slow');
	}



});


$("#strokeColor").click(function(evt) {
	if((canvas.getActiveObject() !== undefined) && (canvas.getActiveObject() !== null)){
		if(canvas.getActiveObject().get("type") === "circle"   || 
			canvas.getActiveObject().get("type") === "rect"     ||
			canvas.getActiveObject().get("type") === "triangle" ||
			canvas.getActiveObject().get("type") === "line") 
		{
			$("body").unbind("keydown");
			$(".color-picker").hide();
			$("#fillColorContainer").hide();
			$("#selectStrokeColor").show();

		}else{
			$("#customMessage p").html("<span class='fa fa-info-circle'></span> Please select a shape");
			$('#customMessage').fadeIn('slow').delay(2000).fadeOut('slow');
		}
	}else{
		$("#customMessage p").html("<span class='fa fa-info-circle'></span> Please select a shape");
		$('#customMessage').fadeIn('slow').delay(2000).fadeOut('slow');
	}
});


$("body").click(function(evt) {

	if(evt.target.id == "addTextContainer")
		return;
	if($(evt.target).closest('#addTextContainer').length)
		return;    

	if(evt.target.id == "selectBrushColor")
		return;
	if($(evt.target).closest('#selectBrushColor').length)
		return;    

	if(evt.target.id == "brushColor")
		return;
	if($(evt.target).closest('#brushColor').length)
		return;  

	if(evt.target.id == "fillColor")
		return;
	if($(evt.target).closest('#fillColor').length)
		return;  

	if(evt.target.id == "fillColorContainer")
		return;
	if($(evt.target).closest('#fillColorContainer').length)
		return;  

	if(evt.target.id == "strokeColor")
		return;
	if($(evt.target).closest('#strokeColor').length)
		return;  

	if(evt.target.id == "selectStrokeColor")
		return;
	if($(evt.target).closest('#selectStrokeColor').length)
		return; 

	if(evt.target.id == "selectCanvasColor")
		return;
	if($(evt.target).closest('#selectCanvasColor').length)
		return; 

	if(evt.target.id == "canvasColor")
		return;
	if($(evt.target).closest('#canvasColor').length)
		return; 

	if(evt.target.id == "ToggleNavBar")
		return;
	if($(evt.target).closest('#ToggleNavBar').length)
		return; 

	if(evt.target.id == "dragDropOverlay")
		return;
	if($(evt.target).closest('#dragDropOverlay').length)
		return; 

	if(evt.target.id == "welcomeBox")
		return;
	if($(evt.target).closest('#welcomeBox').length)
		return; 

	$(".color-picker").hide();
	$("#fillColorContainer").hide();
	$("#dragDropOverlay").hide();
});

/*===================== Setting Color Picker Colors to Relevent DIV =========================*/

function setSelectedTextFillColor(hex){
	$("#textFillColor").css("background-color", "#" + hex);
}

function setTextStrokeColor(hex){
	$("#textStrokeColor").css("background-color", "#" + hex);
}

function setSelectedBrushColor(hex){
	$("#brushColor").css("background-color", "#" + hex);
	$(".ui-widget-header").css("background", "#" + hex);
}

function setSelectedFillColor(hex){
	$("#fillColor").css("background-color", "#" + hex);
}

function setSelectedStrokeColor(hex){
	$("#strokeColor").css("background-color", "#" + hex);
}

/*================= Stoke Settings =====================*/

$("#stokeWidthSlider").slider({
	min: 1,
	max: 20,
	range: "min",
	value: 1,
	slide: function(event, ui) {
		changeShapeStrokeWidth(ui.value);
		$("#stokeWidthLabel").html("Storke Width : " + ui.value);
	}
});


/* enable Stoke */
$("#strokeWidth").click(function(event) {
	canvas.setDimensions({width: canvasWidth, height: canvasHeight});
	if($("#strokeToolPalate").is(":visible") )
		return;

	if((canvas.getActiveObject() !== undefined) && (canvas.getActiveObject() !== null)){
		if(canvas.getActiveObject().get("type") === "circle"   || 
			canvas.getActiveObject().get("type") === "rect"     ||
			canvas.getActiveObject().get("type") === "triangle" ||
			canvas.getActiveObject().get("type") === "line") 
		{
			$("#strokeToolPalate").show("slide", { direction: "left" }, 1000);

			$('#moveTool').removeAttr( "style" );
			isMoveTool = false;
			$('#strokeWidth').css('color', '#d40b0b');
		}else{
			$("#customMessage p").html("<span class='fa fa-info-circle'></span> Please select a shape");
			$('#customMessage').fadeIn('slow').delay(2000).fadeOut('slow');
		}
	}else{
		$("#customMessage p").html("<span class='fa fa-info-circle'></span> Please select a shape");
		$('#customMessage').fadeIn('slow').delay(2000).fadeOut('slow');
	}

});

/* Minimize Stoke */
$("#closeStrokeToolPalate").click(function(event) {

	if($("#strokeToolPalate").is(":visible") ){
		$("#strokeToolPalate").hide("slide", { direction: "left" }, 1000);
	}
	$('#strokeWidth').removeAttr( "style" );

});


/*================= Brush Setting=====================*/
$("#brushSizeSlider").slider({
	min: 1,
	max: 20,
	range: "min",
	value: 1,
	slide: function(event, ui) {
		canvas.freeDrawingBrush.width = ui.value;
		$("#brushSizeLabel").html("Size: " + ui.value);
	}
});

/* enable the pen tool */
$("#brushTool").click(function(event) {
	canvas.setDimensions({width: canvasWidth, height: canvasHeight});
	if($("#brushToolPalate").is(":visible") )
		return;

	canvas.isDrawingMode = true;
	canvas.isTextMode = false;	

	$("#brushToolPalate").show("slide", { direction: "left" }, 1000);

	$('#moveTool').removeAttr( "style" );
	isMoveTool = false;
	$('#brushTool').css('color', '#d40b0b');
});

/* Minimize the pen tool */
$("#closeBrushToolPalate").click(function(event) {

	if($("#brushToolPalate").is(":visible") ){
		$("#brushToolPalate").hide("slide", { direction: "left" }, 1000);
	}

});




//Enable Move Tool
$("#moveTool").click(function(event) {
	canvas.setDimensions({width: canvasWidth, height: canvasHeight});
	if (!isMoveTool) {
		if(canvas.isDrawingMode){
			$("#brushToolPalate").hide("slide", { direction: "left" }, 1000);
			$('#brushTool').removeAttr( "style" );
			canvas.isDrawingMode = false;
		}
		
		$('#moveTool').css('color', '#d40b0b');
		isMoveTool = true;
	}
});


/* Add Border */
$("#addBorder").click(function(event){
	canvas.setDimensions({width: canvasWidth, height: canvasHeight});
	if(addBorder){
		addBorder = false;
		$('#addBorder').removeAttr( "style" );
		$('#addBorder').prop('title', 'Border to View (Recommended)');
		$("#customMessage p").html("<span class='fa fa-info-circle'></span> Border Removed from Canvas");
		$('#customMessage').fadeIn('slow').delay(2000).fadeOut('slow');
	}else{
		addBorder = true;
		$('#addBorder').css('color', '#d40b0b');
		$('#addBorder').prop('title', 'Remove Border');
		$("#customMessage p").html("<span class='fa fa-info-circle'></span> Border Added to Canvas");
		$('#customMessage').fadeIn('slow').delay(2000).fadeOut('slow');
	}
});

/*=================== Clear Canvas ========================*/

$("#clearCanvasBox").click(function(event){
	if(canvas.getActiveObject() !== undefined){
		$('#confirmClearCanvas').modal('show');
	}else{
		$("#customMessage p").html("<span class='fa fa-info-circle'></span> Nothing to clear in canvas");
		$('#customMessage').fadeIn('slow').delay(2000).fadeOut('slow');
	}
});




/*================= Close Welcome Box =====================*/

$(document).on('click', '#closeWelcomeBox', function() {
	$("#welcomeBox").modal("hide");
});


/*================ Help Menu ==========================*/

$(document).on('click', '#feedback', function() {
	$('#feedbackContainer').modal({backdrop: 'static'});
	$('#feedbackContainer').modal('show');

	setTimeout(function(){
		$("input#feedbackEmail").focus();
	}, 600);

});


$(document).on('click', '#aboutUs', function() {
	// $('#aboutUsContainer').modal({backdrop: 'static'});
	$('#aboutUsContainer').modal('show');

});










/*========================== Custom Tooltips =============================*/
// $('.hastip').tooltipsy();

$('.hastip').tooltipsy({
	offset: [+10, 0],
	show: function (e, $el) {
		$el.css({
			'right': parseInt($el[0].style.right.replace(/[a-z]/g, '')) - 500 + 'px',
			'opacity': '0.0',
			'display': 'block'
		}).animate({
			'left': parseInt($el[0].style.left.replace(/[a-z]/g, '')) + 10 + 'px',
			'opacity': '1.0'
		}, 300);
	},
	hide: function (e, $el) {
		$el.slideUp(100);
	}
});





/*================== Media Queries =====================*/
$(window).resize(function(event){
	checkMedia();
});

$(document).on('ready', function(){
	checkMedia();
});

function checkMedia(){
	if (window.matchMedia('(max-width: 1200px)').matches) {
		// console.log("max-width: 1200px");
		$("#leftToolPalate").removeClass('col-md-offset-1');
		canvas.setDimensions({width: canvasWidth, height: canvasHeight});
	} 

	if (window.matchMedia('(min-width: 1201px)').matches) {
		$("#leftToolPalate").addClass('col-md-offset-1');
		$("#canvasSizeMenu").show();
		canvas.setDimensions({width: canvasWidth, height: canvasHeight});
	} 
	if (window.matchMedia('(max-width: 992px)').matches) {

	}

	if (window.matchMedia('(max-width: 992px)').matches) {
		$("#rightToolPalate").removeClass('col-sm-5');
		$("#addImageContainer").addClass("marginTop10px");
		canvas.setDimensions({width: canvasWidth, height: canvasHeight});
	}else{
		$("#rightToolPalate").addClass('col-md-5');
		$("#rightToolPalate").removeClass('col-md-4');
		$("#addImageContainer").removeClass("marginTop10px");
	}

	if (window.matchMedia('(max-width: 550px)').matches) {
		$("#canvasSizeMenu").hide();
		$("#leftToolPalate").removeClass('screen col-md-1 col-sm-1 col-xs-2');
		$("#leftToolPalate").addClass('col-md-12');
		$("#leftToolPalate").addClass('resRow');
	}else{
		$("#canvasSizeMenu").show();
		$("#leftToolPalate").addClass('screen col-md-1 col-sm-1 col-xs-2');
		$("#leftToolPalate").removeClass('col-md-12');
		$("#leftToolPalate").removeClass('resRow');
	}

}

/*============= Hide dragDrop Overlay ==================*/

$("#playGround #dragDropOverlay").click(function(){
	$("#playGround #dragDropOverlay").hide();
});

$("#playGround #dragDropOverlay").on("dragenter", function(){
	$("#playGround #dragDropOverlay").hide();
});

/*============= Crop Saving Image ==================*/

// $("#showDownloadImg").cropper({
//   aspectRatio: 1.618,
//   done: function(data) {
//     // Output the result data for cropping image.
//   }
// });

var cropTool = false;

$("#cropTool").click(function() {
	console.log("cropTool");
	// $("#showDownloadImg").cropper("destroy");
	$("#showDownloadImg").cropper({
		zoomable : false,
		autoCrop : true,
		modal : true,
		done: function(data) {
		}
	});
	cropTool = true;
	$('#saveImageModal .docs-toolbar #cropResetTool').attr("disabled", false);
	$(".saveChangers").show();
  // $("#showDownloadImg").cropper("setDragMode", "crop");
});



$("#cropResetTool").click(function() {
	$("#showDownloadImg").cropper("destroy");
	$('#saveImageModal .docs-toolbar #cropResetTool').attr("disabled", true);
	cropTool = false;
	$(".saveChangers").hide();
});


$("#saveCropImg").click(function() {
	
	var cropedImgUrl = $("#showDownloadImg").cropper("getDataURL","image/png");
	console.log(cropedImgUrl);
	$("#showDownloadImg").cropper("replace", cropedImgUrl);
	$("#downloadImage").attr("href", cropedImgUrl);
	$("#downloadImage").attr("download", "meme.png");

	setTimeout(function(){
		$("#showDownloadImg").cropper("destroy");
		console.log("destroy11");
	}, 100);
	$('#saveImageModal .docs-toolbar #cropResetTool').attr("disabled", true);
	cropTool = false;
	$(".saveChangers").hide();
	console.log("destroy");
});

$("#closeSaveModal").click(function() {
	if(cropTool){
		$("#showDownloadImg").cropper("destroy");
		cropTool = false;
	}
});





/*============ Disable Right Clicks on header =================*/

$('#header').bind('contextmenu', function(e) {
    return false;
}); 