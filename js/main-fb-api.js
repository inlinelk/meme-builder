var userId = "2345";
var userName = "common";
var userAvatar;
var accessToken;
var imageLocation;
var queryUrl;

    //var canvas = new fabric.Canvas('canvas', 
    //              {
    //                  backgroundColor: "#ffffff"
    //              });
        //callback function that is called to check the user's login status
        function statusChangeCallback(response) {
            //logged into facebook and the app
            if (response.status == "connected") {
                userId = response.authResponse.userID;
                accessToken = response.authResponse.accessToken;
                onUserLogin();
            } else if (response.status == "not_authorized") { //logged into facebook, but the app isn't authorised

                //alert("Without authorising this app, you cannot publish any images you have created.");
                FB.login(
                    function(response){
                        //console.log("logging in user");
                        if(response.status == "connected") {
                            userId = response.authResponse.userID;
                            accessToken = response.authResponse.accessToken;
                            onUserLogin();
                        }
                        else if(response.status == "not_authorized") {
                            // alert("Without authorising this app, you cannot publish any images you have created.");
                            $('#postingImage').modal('hide');
                            $('#popUpUnauthorised').fadeIn('slow').delay(4000).fadeOut('slow');
                        } 
                        else {
                            alert("Without login into facebook, you cannot publish any images you have created.");
                            $('#postingImage').modal('hide');
                            $('#popUpNotLogedin').fadeIn('slow').delay(4000).fadeOut('slow');
                        }
                    }, 
                    {scope: 'public_profile'}
                    );

            } else { //not logged into facebook, prompt to login to facebook
                $('#postingImage').modal('hide');
                // console.log("not logged into facebook");
                // $("#loginPrompt").show();
                $("#customMessage p").html("<span class='fa fa-info-circle'></span> Please login to facebook <fb:login-button scope='public_profile,email' onlogin='checkLoginState();'></fb:login-button>");
                $('#customMessage').fadeIn('slow');
            }
        }
        
        //Facebook initialiser
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '883133271720520',
                xfbml      : true,
                version    : 'v2.1'
            });
            // console.log("initing FB API");
            /*FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });*/
};

(function(d, s, id){
    // console.log("loading the facebook api..");
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function onUserLogin() {
            //console.log(userId);
            var reqPath = "/me/permissions";
            var publishGranted = false;
            //check if the user has given publish_actions permission
            FB.api(reqPath, function(res) {
                // console.log("permissions granted: ");
                $.each(res.data, function(key, value) {
                    // console.log(key + " : " + value);
                    if(value.permission == "publish_actions") { //permission has been asked, but not granted
                        if(value.status != "granted") {
                            publishGranted = false;
                        }
                        else if(value.status == "granted") {
                            publishGranted = true;
                        }
                    }
                    else { //permission has not been asked at all
                        publishGranted = false;
                    }
                    //NOTE: Two conditions are checked here because of the nature of Facebook's response 
                });     

                if(publishGranted === false) { //if we dont have permission to upload to user's profile
                    //show request for publish actions
                $('#postingImage').modal('hide');
                $("#popUpPermission").show();
            }
                else { //we have permission to upload pictures to the user's account
                    //upload image to server
                // console.log("saving to server..");
                var canvasNative = document.getElementById("canvas");
                var img = canvasNative.toDataURL("image/png");
                var flag = false;
                imageLocation = "https://www.inline.lk/meme-generator/";
                    //ajax image base 64 data and other required fields to the PHP endpoint
                    $.ajax({
                        url: "uploadImageToFacebook.php",
                        type: "POST",
                        data: {
                            image_data: img,
                            user_name: userName,
                            user_id: userId,
                            access_token: accessToken
                        },
                        error: function(reqObj, status, exp) {
                            // console.log(status);
                            // console.log(exp);
                        },
                        success: function(data, status, reqObj) {
                            // console.log(status);
                            // console.log(data);
                            // console.log(reqObj);
                            if(data != "false") {
                                // alert("Image posted to facebook!");
                                //FB.logout();
                                $('#postingImage').modal('hide');
                                $('#popUpImagePosted').fadeIn('slow').delay(4000).fadeOut('slow');
                            }
                        }
                    });
}           
});
}

        //this function will post the image to the user's timeline
        function postToUserTimeline() {
            $('#postingImage').modal({backdrop: 'static'});
            $('#postingImage').modal('show');
            // console.log("attempting to share on facebook..");
            //deactivate canvas and re-render all elements
            canvas.deactivateAll().renderAll();
            //check FB login
            FB.login(
                function(response){
                    //console.log("logging in user");
                    if(response.status == "connected") { //logged into facebook and app is authorised
                        userId = response.authResponse.userID;
                        accessToken = response.authResponse.accessToken;
                        onUserLogin();
                    }
                    else if(response.status == "not_authorized") { //logged into facebook, app isn't authorised
                        // alert("Without authorising this app, you cannot publish any images you have created.");
                    $('#postingImage').modal('hide');
                    $('#popUpUnauthorised').fadeIn('slow').delay(4000).fadeOut('slow');
                } 
                    else { //not logged into facebook and the app
                        //alert("Without login into facebook, you cannot publish any images you have created.");
                        $('#postingImage').modal('hide');
                        $('#popUpNotLogedin').fadeIn('slow').delay(4000).fadeOut('slow');
                    }
                }, 
                {scope: 'public_profile,publish_actions'}
                );
}

function requestPublishActions(){
    $('#popUpPermission').fadeOut('slow');
    // console.log("getting permissions..");
    FB.login(
        function(res) {
                    if(res.status == "connected") { //permission has been asked and a response has been received successfully
                        var permissions = res.authResponse.grantedScopes;
                        permissions = permissions.split(",");
                        var secondCheckPublicPermissions = false;
                        //go through each permission they have granted and look for publish_actions
                        $.each(permissions, function(key, value) {
                            // console.log("value: " + value);
                            if(value == "publish_actions") { //publish_actions has been granted
                                secondCheckPublicPermissions = true;
                            }
                            else { //not granted
                                secondCheckPublicPermissions = false;
                            } 
                        }); 

                        if(secondCheckPublicPermissions) { //start uploading the image if permission has been granted
                            //upload image to server
                            // console.log("saving to server..");
                            var canvasNative = document.getElementById("canvas");
                            var img = canvasNative.toDataURL("image/png");
                            var flag = false;
                            imageLocation = "https://www.inline.lk/meme-generator/";
                            //ajax image base 64 data and other required fields to the PHP endpoint
                            $.ajax({
                                url: "save_image.php",
                                type: "POST",
                                data: {
                                    image_data: img,
                                    user_name: userName,
                                    user_id: userId,
                                    access_token: accessToken
                                },
                                error: function(reqObj, status, exp) {
                                    // console.log(status);
                                    // console.log(exp);
                                },
                                success: function(data, status, reqObj) {
                                    // console.log(status);
                                    // console.log(data);
                                    // console.log(reqObj);
                                    if(data != "false") {
                                        // alert("Image posted to facebook!");
                                        $('#postingImage').modal('hide');
                                        $('#popUpImagePosted').fadeIn('slow').delay(4000).fadeOut('slow');
                                    }
                                }
                            });
}
                        else { //publish_actions permission has not been granted
                            // alert("You must give me permission to post to Facebook");
                            $('#postingImage').modal('hide');
                            $('#popUpPermissionFail').fadeIn('slow').delay(4000).fadeOut('slow');
                        }
                    }                           
                },
                {
                    scope: "publish_actions", 
                    return_scopes: true
                }
                );
}

$("#shareFacebook").click(postToUserTimeline);
$("#grantPermissions").click(requestPublishActions);