/*
*
*
*
*
*
*/

var canvas = new fabric.Canvas('canvas', {
    backgroundColor: "#ffffff"
//rotationCursor: 'url("../images/rotate-icon.png")'
});

// canvas.rotationCursor = url("../images/rotate-icon.png");
// console.log(canvas.rotationCursor);
// console.log(canvas.item(0));

canvas.setDimensions({width: canvasWidth, height: canvasHeight});


/*============== Set Color Pickers ==============================*/
$("#selectTextFillColor").colpick({
    flat: true,
    layout: 'hex',
    submit: 0,
    onChange: function(hsb, hex) {
       // console.log("fill colour change");
    //textFillColor = "#" + hex;
    $("#textToAdd").attr("data-fill", "#" + hex);
    //$("textToAdd").data("fill", "#" + hex);
    setSelectedTextFillColor(hex);
}
});

$("#selectTextStrokeColor").colpick({
    flat: true,
    layout: 'hex',
    submit: 0,
    onChange: function(hsb, hex) {
    //textStrokeColor = "#" + hex;
    $("#textToAdd").attr("data-stroke", "#" + hex);
    setTextStrokeColor(hex);
}
});

$("#selectFillColor").colpick({
    flat: true,
    layout: 'hex',
    submit: 0,
    color: getCurrentObjectFillColour(), 
    onChange: function(hsb, hex, rgb, el, bySetColor) {
        //this condition must be checked because this event is fired by colpicSetColor() as well
        //in that case, it automatically sets the fill colour of the object to fff without transparency
        //using this condition will fire this code if the colour is changed by the user explicitly
        if(!bySetColor) {
           $("#transparentFill").attr("checked", false);
         // console.log("hex:");
         // console.log(hex);
         changeShapeFillColour("#" + hex, this);
         setSelectedFillColor(hex);
     }
     else {
            setSelectedFillColor(hex); //sets the colour of the colour picker to the hex colour
        }
    }
});

$("#selectStrokeColor").colpick({
    flat: true,
    layout: 'hex',
    submit: 0,
    onChange: function(hsb, hex) {
       changeShapeStrokeColour("#" + hex);
       setSelectedStrokeColor(hex);
   }
});

$("#selectBrushColor").colpick({
    flat: true,
    layout: 'hex',
    submit: 0,
    onChange: function(hsb, hex) {
        canvas.freeDrawingBrush.color = "#" + hex;
        setSelectedBrushColor(hex);
    }
});


$("#selectCanvasColor").colpick({
    flat: true,
    layout: 'hex',
    submit: 0,
    onChange: function(hsb, hex) {
        changeCanvasBackgroundColour("#" + hex);
    }
});

/*================= add the image to the canvas when clicked on =================*/
$(document).on('click', '.stockImageContainer', function(event) {
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
    event.preventDefault();
    var imageId = $(this).children("img")[0]["id"];
    var imageElement = document.getElementById(imageId);
    var image = new fabric.Image(imageElement, {
        left: 30 + imageAddOffset,
        top: 80 + imageAddOffset,
        width: 300,
        height: 300
    });
    canvas.add(image);
    canvas.isDrawingMode = false;
    canvas.isTextMode = false;  
    isMoveTool = true;
    if($("#brushToolPalate").is(":visible") ){
        $("#brushToolPalate").hide("slide", { direction: "left" }, 1000);
        $('#brushTool').removeAttr("style");
    }
    imageAddOffset += 10;
});

/*=================== add text to the canvas ===========================*/
$("#addText").click(function(event){
    event.preventDefault();
    var inputText = $("#textToAdd").val();
    var textInstance = new fabric.Text(inputText, {
        fill: $("#textToAdd").attr("data-fill"),
        stroke: $("#textToAdd").attr("data-stroke"),
        top: parseInt($("#textToAdd").attr("data-top")),
        left: parseInt($("#textToAdd").attr("data-left"))
    });

// var lang = $("#fontSelector").val();
// if(lang == "en") {
    textInstance.setFontFamily("Impact");
// }
// else if(lang == "si") {
//     textInstance.setFontFamily("lankadweepa");
// }
// else if(lang == "ta") {
//     textInstance.setFontFamily("tamil_web");
// }
deleteObjectOnCanvas(canvas.getActiveObject());
canvas.add(textInstance);

if($("#brushToolPalate").is(":visible") ){
    $("#brushToolPalate").hide("slide", { direction: "left" }, 1000);
    $('#brushTool').removeAttr("style");
}
canvas.isDrawingMode = false;
canvas.isTextMode = false;  
isMoveTool = true;
$("#addTextContainer #addText").text("Add Text");
canvas.setDimensions({width: canvasWidth, height: canvasHeight});
});



//allow user to add a circle
$("#addCircle").click(function(event) {
    var circle = new fabric.Circle({
        radius: 50,
        left: 50,
        top: 50,
        fill: "rgba(0,0,0,0)",
        stroke: "#000"
    })
    canvas.add(circle);
    canvas.isDrawingMode = false;
    canvas.isTextMode = false;  
    isMoveTool = true;
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
});

//allow user to add a square
$("#addSquare").click(function(event) {
    var rect = new fabric.Rect({
        width: 50,
        height: 50,
        left: 50,
        top: 50,
        fill: "rgba(0,0,0,0)",
        stroke: "#000"
    })
    canvas.add(rect);
    canvas.isDrawingMode = false;
    canvas.isTextMode = false;  
    isMoveTool = true;
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
});

//allow user to add a square
$("#addTriangle").click(function(event) {
    var triangle = new fabric.Triangle({
        width: 50,
        height: 50,
        left: 50,
        top: 50,
        fill: "rgba(0,0,0,0)",
        stroke: "#000"
    })
    canvas.add(triangle);
    canvas.isDrawingMode = false;
    canvas.isTextMode = false;  
    isMoveTool = true;
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
});

//allow user to add a horizontal line
$("#addHorLine").click(function(event) {
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
    var line = new fabric.Line([100, 100, 300, 100], {
        strokeWidth: 4,
        stroke: "#000"
    })
    canvas.add(line);
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
});

//allow user to add a vertical  line
$("#addVerLine").click(function(event) {
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
    var line = new fabric.Line([100, 100, 100, 300], {
        strokeWidth: 4,
        stroke: "#000"
    })
    canvas.add(line);
    canvas.isDrawingMode = false;
    canvas.isTextMode = false;  
    isMoveTool = true;
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
});


//delete item from canvas
$("#deleteItem").click(function(event) {
    event.preventDefault();
    canvas.remove(canvas.getActiveObject());
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
//console.log();
});

//clear canvas
$("#clearCanvas").click(function(event) {
    event.preventDefault();
    canvas.clear();
    canvas.setDimensions({width: canvasWidth, height: canvasHeight});
    $('#confirmClearCanvas').modal('hide');
});



//save canvas to image
$("#saveAsPng").click(function(event) {
    $('#saveImageModal #downloadImage').hide();
    $('#saveImageModal #cropImg').hide();
    $("#saveImageModal #generateImage").show();
    $('#saveImageModal .showDownloadImgContainer').hide();
    $('#saveImageModal .docs-toolbar').hide();
    $('#saveImageModal').modal({backdrop: 'static'});
    $('#saveImageModal').modal('show');
});


$("#generateImage").click(function(event) {
	event.preventDefault();
    if($("#saveImageToGallery").is(":checked")){
        $("#generateImage").addClass('active');
        // console.log("checked");
        var user = "anonymous";
        canvas.deactivateAll().renderAll();
        var canvasNative = document.getElementById("canvas");
        var img = canvasNative.toDataURL("image/png");

        $.ajax({
        	url : "saveImage.php",
        	type : "POST",
        	data : {
        		username : user,
        		image_data : img
        	},
        	success : function(data, status, requestObj) {
        	  // console.log(data); //check if data is equal to true or false for errors
              saveCanvasToImage();
          },
          error : function(requestObj, message, exception) {
          // console.log(requestObj);
          // console.log(message);
          // console.log(exception);
      }
  });
    }else{
        $("#generateImage").addClass('active');
        saveCanvasToImage();
    }
});


// $("#saveAsPng").click(function(event) {
    function saveCanvasToImage(){
        // console.log("to save");

        /*add border around the created image if required*/
        var border;
        if(addBorder) {
            border = new fabric.Rect({
                x: 0,
                y: 0,
                height: canvasWidth-1,
                width: canvasWidth-1,
                fill: "#ffffff",
                stroke: "#000000",
                lockMovementX: true,
                lockMovementY: true
            });
            canvas.add(border);
            canvas.moveTo(border, -999);
        }

        /*clear all selections before saving the image*/
        canvas.deactivateAll().renderAll();

        /*native js code to deal with canavs element better*/
        var canvasNative = document.getElementById("canvas");
        var img = canvasNative.toDataURL("image/png");
        img = img.replace("data:image/png", "data:application/octet-stream");
        //console.log(img);

        $("#showDownloadImg").attr("src", img);
        $("#downloadImage").attr("href", img);
        $("#downloadImage").attr("download", "meme.png");
        
        var loadDwnBtn = setInterval(function(){
            // console.log("Interval");
            if($("#downloadImage").attr("href") == "#"){
                return;
            }else{
             $("#generateImage").removeClass('active');
             $("#generateImage").hide();
             $('#saveImageModal #downloadImage').show();
             $('#saveImageModal .showDownloadImgContainer').show();
             $('#saveImageModal #cropImg').show();
             $('#saveImageModal .docs-toolbar').show();
             $('#saveImageModal .docs-toolbar #cropResetTool').attr("disabled", true);
             clearInterval(loadDwnBtn);
         }
     }, 1000);
//window.location = img;
}
// });

$("#downloadImage").click(function(event) {
    setTimeout(function(){
        $('#saveImageModal').modal('hide');
    }, 1000);
});

/*allow  user to upload their own images*/
$("#addNewImage").click(function() {

    /*native JS code, due to lack of support by jQuery*/
    var reader = new FileReader();
    var fileInput = document.getElementById("imageUpload");

    if(fileInput.files[0] != null) {
        // console.log("file upload detected");

        /*validate the type of file*/
        var type = fileInput.files[0].type;
        type = type.split("/")[0];
        if(type == "image") {
            // console.log("file is an image");

            reader.onloadend = function(data) {
                if(reader.error) {
                    alert(reader.error);
                }
                else {
                    var temp = new Image();
                    temp.src = reader.result;

                    fabric.Image.fromURL("" + reader.result, function(img) {
                        //change image width and height depending on the size of the image
                        if(temp.width > 500 || temp.height > 500) {
                            img.width = (temp.width * 0.5);
                            img.height = (temp.height * 0.5);
                        }

                        img.left = 10 + imageAddOffset;
                        img.top = 10 + imageAddOffset;

                        canvas.add(img);
                        canvas.deactivateAll().renderAll();
                        imageAddOffset += 10;
                        canvas.setDimensions({width: canvasWidth, height: canvasHeight});
                    });
                }
            }
            reader.readAsDataURL(fileInput.files[0]);
            // console.log(fileInput.files[0]);
        }

    }
    else {
        alert("No files selected!");
    }
});



/*
* get keyboard events from the canvas when an object / group of objects
* are selected
* - delete key
* - arrow keys
*/
canvas.on("object:selected", function(event) {
    // console.log(canvas.getActiveGroup());
    if(canvas.getActiveGroup() === null) {
        // console.log("selected an object");
        //console.log(canvas.getActiveObject().get("type"));
        $("body").on("keydown", function(event) {
            event.preventDefault();
            // console.log("keyCode: " + event.keyCode);
            if(event.keyCode === 8 || event.keyCode === 46) {
                deleteObjectOnCanvas(canvas.getActiveObject());
                $("body").unbind("keydown");    
            }
            else if(event.keyCode === 38) { //up arrow key
                moveObjectOnCanvasUp(canvas.getActiveObject());
            }
            else if(event.keyCode === 40) { //down arrow key
                moveObjectOnCanvasDown(canvas.getActiveObject());
            }
            else if(event.keyCode === 37) { //left arrow key
                moveObjectOnCanvasLeft(canvas.getActiveObject());
            }
            else if(event.keyCode === 39) { //right arrow key
                moveObjectOnCanvasRight(canvas.getActiveObject());
            }
            else if(event.keyCode === 27) { //escape(esc) key
            	canvas.deactivateAll().renderAll();
                $("body").unbind("keydown");    
            }
        });

if(canvas.getActiveObject().get("type") === "circle"   || 
    canvas.getActiveObject().get("type") === "rect"     ||
    canvas.getActiveObject().get("type") === "triangle") 
{
    // console.log("getCurrentObjectFillColour(): ");
    // console.log(getCurrentObjectFillColour());
            //change fill and stroke colour to the object's colour
            $("#selectFillColor").colpickSetColor(getCurrentObjectFillColour(), true);
            $("#selectStrokeColor").colpickSetColor(getCurrentObjectStrokeColour(), true);
        }

        if(canvas.getActiveObject().get("type") === "line") 
        {
            //change fill and stroke colour to the object's colour
            $("#selectStrokeColor").colpickSetColor(getCurrentObjectStrokeColour(), true);
        }

        $("#canvasContainer").on("dblclick", function(event) {
           event.preventDefault();
           // console.log(canvas.getActiveObject());
           if(canvas.getActiveObject().get("type") === "text") {
              $("#textToAdd").attr("data-top", canvas.getActiveObject().originalState.top);
              $("#textToAdd").attr("data-left", canvas.getActiveObject().originalState.left);
              $("#canvasContainer").unbind("dblclick");
              $("body").unbind("keydown");
              $("#textToAdd").val(canvas.getActiveObject().originalState.text);
              $("#addTextContainer").modal("show");
              $("#addTextContainer #addText").text("Edit Text");
          }
      })
    }
});

canvas.on("selection:cleared", function(event) { 
    $("body").unbind("keydown");
    $("#canvasContainer").unbind("dblclick");
});


//actions for a whole selected group
canvas.on("selection:created", function(event) { 
//delete selected group
    // console.log("created a selection");
    //console.log(canvas.getActiveGroup());
});

//remove bindings and deactivate canvas before adding new text
$(document).on("click", "#textTool", function(event) {
    // console.log("clicked");
    canvas.deactivateAll().renderAll();
    $("body").unbind("keydown");
    $("#canvasContainer").unbind("dblclick");
    $("#textToAdd").attr("data-top", 50);
    $("#textToAdd").attr("data-left", 50);
});

//create the drag and drop functionality for the canvas
$("#canvasContainer").on("dragenter", function(event) {
    // console.log("drag entered");
    event.stopPropagation();
    event.preventDefault();
});

$("#canvasContainer").on("dragover", function(event) {
    event.stopPropagation();
    event.preventDefault();
});

$("#canvasContainer").on("drop", function(event) {
    event.stopPropagation();
    event.preventDefault();
    // console.log("dropped: ");
    // console.log(event);

    var reader = new FileReader();

    reader.onloadend = function(data) {
        if(reader.error) {
            alert(reader.error);
        }
        else {
            var temp = new Image();
            temp.src = reader.result;
            fabric.Image.fromURL("" + reader.result, function(img) {
                //change image width and height depending on the size of the image
                if(temp.width > 500 || temp.height > 500) {
                    img.width = (temp.width * 0.5);
                    img.height = (temp.height * 0.5);
                }

                img.left = 10 + imageAddOffset;
                img.top = 10 + imageAddOffset;

                canvas.add(img);
                canvas.deactivateAll().renderAll();
                imageAddOffset += 10;
                //resets the canvas size to fix the image positioning bug in canvas
                canvas.setDimensions({width: canvasWidth, height: canvasHeight});
            });
        }
    }

    reader.readAsDataURL(event.originalEvent.dataTransfer.files.item(0));
    // console.log(event.originalEvent.dataTransfer.files.item(0));
});

$("#transparentFill").on("click", function(event) {
    // console.log(event.target.checked);
    if(event.target.checked) {
        $("#fillColor").css("background-color", "#fff");
        changeShapeFillColour("rgba(0,0,0,0)");
    }
});

$("#bringObjectFront").on("click", function(event) {
    event.preventDefault();
    bringObjectFront();
});

$("#sendObjectBack").on("click", function(event) {
    event.preventDefault();
    sendObjectBack();
});

$("#flipImage").on("click", function(event) {
	event.preventDefault();
	flipImageOnXAxis();
});

// ---  Utility Functions  --- //

function deleteObjectOnCanvas(activeObject) {
    canvas.remove(activeObject);
    canvas.renderAll();
}

function moveObjectOnCanvasUp(activeObject) {
    activeObject.top -= 5;
    canvas.renderAll();
}

function moveObjectOnCanvasDown(activeObject) {
    activeObject.top += 5;
    canvas.renderAll();
}

function moveObjectOnCanvasLeft(activeObject) {
    activeObject.left -= 5;
    canvas.renderAll();
}

function moveObjectOnCanvasRight(activeObject) {
    activeObject.left += 5;
    canvas.renderAll();
}

//changes the fill colour of the active object if the object is a circle, rect or triangle
function changeShapeFillColour(colour) {
//console.log(canvas.getActiveObject().get("type"));
//console.log(colour);
if(canvas.getActiveObject() !== null) {
	if(canvas.getActiveObject().get("type") === "circle" || 
     canvas.getActiveObject().get("type") === "rect"   ||
     canvas.getActiveObject().get("type") === "triangle") 
	{
		canvas.getActiveObject().setFill(colour);
		canvas.renderAll();
	}
	else {
		//alert("Oops. Changing the fill colour of that shape isn't supported :(");
		//canvas.deactivateAll().renderAll();
	}
}
}

//changes the stroke colour of the active object if the object is a circle, rect or a line
function changeShapeStrokeColour(colour) {
//console.log(canvas.getActiveObject().get("type"));
//console.log(colour);
if(canvas.getActiveObject() !== null) {

	if(canvas.getActiveObject().get("type") === "circle"   || 
        canvas.getActiveObject().get("type") === "rect"     ||
        canvas.getActiveObject().get("type") === "triangle" ||
        canvas.getActiveObject().get("type") === "line") 
	{
		canvas.getActiveObject().setStroke(colour);
		canvas.renderAll();
	}
	else {
		//alert("Oops. Changing the stroke colour of that shape isn't supported :(");
		//canvas.deactivateAll().renderAll();
	}
}
}

//change stroke width value if object is circle, rect, triangle or a line
function changeShapeStrokeWidth(size) {
//console.log(canvas.getActiveObject().get("type"));
//console.log(colour);
if(canvas.getActiveObject() !== null) {
	if(canvas.getActiveObject().get("type") === "circle"   || 
        canvas.getActiveObject().get("type") === "rect"     ||
        canvas.getActiveObject().get("type") === "triangle" ||
        canvas.getActiveObject().get("type") === "line") 
	{
		canvas.getActiveObject().setStrokeWidth(size);
		canvas.renderAll();
	}
	else {
		//alert("Oops. Changing the stroke colour of that shape isn't supported :(");
		//canvas.deactivateAll().renderAll();
	}
}
}


//changes the background colour of the canvas
function changeCanvasBackgroundColour(color) {
//console.log("changing background color to: ");
//console.log(color);
canvas.setBackgroundColor(color, function() {
	canvas.renderAll();
});
}

function sendObjectBack() {
    if(canvas.getActiveObject() !== null) {
       canvas.sendBackwards(canvas.getActiveObject());
   }
}

function bringObjectFront() {
	if(canvas.getActiveObject() !== null) {
		canvas.bringForward(canvas.getActiveObject());
	}
}

function flipImageOnXAxis() {
	if(canvas.getActiveObject() !== null) {
		canvas.getActiveObject().flipX = !canvas.getActiveObject().flipX; 
		canvas.renderAll();
	}
}

function flipImageOnYAxis() {
	if(canvas.getActiveObject() !== null) {
		canvas.getActiveObject().flipY = !canvas.getActiveObject().flipY; 
		canvas.renderAll();
	}
}

//checks if an obejct is selected and returns the fill and stroke colours
function getCurrentObjectFillColour() {
    if(canvas.getActiveObject() !== undefined && canvas.getActiveGroup() === null) {
        if(canvas.getActiveObject().get("type") === "circle"   || 
            canvas.getActiveObject().get("type") === "rect"     ||
            canvas.getActiveObject().get("type") === "triangle")
        {
            // console.log("canvas.getActiveObject().get(fill): ");
            // console.log(canvas.getActiveObject().get("fill"));
            if(canvas.getActiveObject().get("fill") === "rgba(0,0,0,0)") {
                changeShapeFillColour("rgba(0,0,0,0)");
                return {r:255, g:255, b:255, a:0};
            }

            return canvas.getActiveObject().get("fill");
        }
        else {
            //return "rgba(0,0,0,0)";
            // console.log("attempting to detect colours... no active object currently");
        }
    }
}

function getCurrentObjectStrokeColour() {
    if(canvas.getActiveObject() !== undefined && canvas.getActiveGroup() === null) {
        if(canvas.getActiveObject().get("type") === "circle"    || 
            canvas.getActiveObject().get("type") === "rect"     ||
            canvas.getActiveObject().get("type") === "triangle" ||
            canvas.getActiveObject().get("type") === "line")
        {
            // if(canvas.getActiveObject().get("stroke") === "rgba(0,0,0,0)") {
            //  return {r:255, g:255, b:255};
            // }

            return canvas.getActiveObject().get("stroke");
        }
        else {
            //return "rgba(0,0,0,0)";
            // console.log("attempting to detect colours... no active object currently");
        }
    }
}



/*===================== Submit Feedback =============================*/

$(document).on('click', '#feedbackSubmit', function() {
    $(this).addClass("active");
    //Feedback logic
    var mail = $("#feedbackEmail").val();
    var sub = $("#feedbackSubject").val();
    var msg = $("#feedbackMessage").val();
    $.ajax({
        type: "POST",
        url: "../../mailer.php",
        data: {
            sentFrom: mail,
            subject: sub,
            message: msg,
            requestParty: "Meme Builder"
        },
        success: function(data, status, requestObject) {
            // console.log(data);
            // console.log(status);
            $("#feedbackSubmit").removeClass("active");
            $('#feedbackContainer').modal('hide');
            $('#feedbackSuccessMessage').fadeIn('slow').delay(4000).fadeOut('slow');
        },
        error: function(requestObject, status, exception) {
            // alert(status);
            // console.log(requestObject);
            // console.log(exception);
            $("#feedbackSubmit").removeClass("active");
            $('#feedbackContainer').modal('hide');
            $('#feedbackFailMessage').fadeIn('slow').delay(4000).fadeOut('slow');
        }
    }); 
});