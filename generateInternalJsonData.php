<?php
class GenerateInternalJsonData {
	
	private $directory;
	private $outputFile;

	public function __construct($dir, $out) {
		$this->directory = $dir;
		$this->outputFile = $out;
	}

	public function generate() {
		$arrayStore;
		$index = 0;
		$allFiles = scandir($this->directory);
		foreach ($allFiles as $key => $value) {

			if($value != "." && $value != ".." && $value != ".DS_Store" ) {
				//var_dump($value);
				$temp = explode(".", $value);
				//var_dump($temp);
				$arrayStore[$index]["id"] = $temp[0];
				$arrayStore[$index]["url"] = $value;
				$index++;
			}
		}

		$writeHandler = fopen($this->outputFile, "w");
		fwrite($writeHandler, json_encode($arrayStore));

		if(!fclose($writeHandler)) {
			return false;
		}
		
		return true;
	}

	public function __destruct() {

	}

}

//test the class
$memeJsonGenerator = new GenerateInternalJsonData("images/blankMemes", "blankMemes.json");
echo $memeJsonGenerator->generate();

$speechbubblesJsonGenerator = new GenerateInternalJsonData("images/speechbubbles", "speechbubbles.json");
echo $speechbubblesJsonGenerator->generate();

$templatesJsonGenerator = new GenerateInternalJsonData("images/templates", "templates.json");
echo $templatesJsonGenerator->generate();
?>