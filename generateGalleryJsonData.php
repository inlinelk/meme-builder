<?php
class GenerateGalleryJsonData {
	
	private $directory;
	private $outputFile;

	public function __construct($dir, $out) {
		$this->directory = $dir;
		$this->outputFile = $out;
	}

	public function generate() {
		$arrayStore;
		$index = 0;
		$allFiles = scandir($this->directory);
		foreach ($allFiles as $key => $value) {

			if($value != "." && $value != ".." && $value != ".DS_Store" ) {
				
				$temp = explode("_", $value);
				//var_dump($temp);
				$arrayStore[$index]["username"] = $temp[0];
				$arrayStore[$index]["date"] = $temp[1];
				$arrayStore[$index]["url"] = $value;
				$index++;
			}
		}

		$writeHandler = fopen($this->outputFile, "w");
		fwrite($writeHandler, json_encode($arrayStore));

		if(!fclose($writeHandler)) {
			return false;
		}
		
		return true;
	}

	public function __destruct() {

	}

}

//test the class
// $jsonGenerator = new GenerateJsonData("uploads/");
// echo $jsonGenerator->generate();
?>