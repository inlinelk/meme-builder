<?php
require_once "generateGalleryJsonData.php";

class SaveImage {

	private $imageData;
	private $username;

	public function __construct($image, $user) {
		$this->imageData = $image;
		$this->username = $user;

		$this->imageData = str_replace("data:image/png;base64,", "", $this->imageData);
	}

	public function writeToFile() {
		$jsonGenerator = new GenerateGalleryJsonData("uploads/", "galleryInfo.json");
		
		//generate the filename
		$shaString = $this->username;
		$shaString = str_shuffle($shaString);
		$filename = "uploads/";
		$filename .= $this->username ."_" . date("d-m-Y") . "_";
		$filename .= hash("crc32b", $shaString) . ".png";
		//open output file
		$outputFile = fopen($filename, "w");
		//write to file
		fwrite($outputFile, base64_decode($this->imageData));

		// var_dump($shaString);
		// var_dump($filename);
		// var_dump($this->imageData);
		// var_dump($this->username);

		if(!fclose($outputFile)) {
			return "false";
		}
		
		if(!$jsonGenerator->generate()) {
			return "false";	
		}
		//echo "saved image on server..";

		$returnObj[0] = true;
		$returnObj[1] = $filename;
		
		return $returnObj;
	}

	public function __destruct() {

	}

}

//test the class
// if($_SERVER["REQUEST_METHOD"] == "POST") {
// 	$saveImage = new SaveImage($_POST['image_data'], $_POST['username']);
// 	echo $saveImage->writeToFile();
// }
?>
